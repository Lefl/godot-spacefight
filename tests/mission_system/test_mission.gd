extends WAT.Test

var root: MapSceneRoot
var player: PlayerShip
var mission: Mission
var stage: MissionStage
var objective: MissionObjective


func title() -> String:
	return "Test Mission System"


func pre() -> void:
	root = MapSceneRoot.new()
	print("[TEST] Ignore next messages about nodes not being found.")

	mission = Mission.new()
	stage = MissionStage.new()
	mission.add_child(stage, true)
	objective = MissionObjectiveDummy.new()
	objective.description = "TESTINGTESTINGATTENTIONPLEASE"
	stage.add_child(objective)

	mission.add_child(MissionStageEnd.new())
	mission.add_child(MissionStageFail.new())
	root.add_child(mission)

	player = load("res://actors/ipc/player_ship/player.tscn").instance()
	root.add_child(player)
	add_child(root)
	player.owner = root
	mission.owner = root
	root.mission = mission
	root.local_player = player


func post():
	root.free()


func test_mission_root_setup() -> void:
	describe("Mission root node initialization")

	watch(mission, "mission_initialized")
	mission.init_mission(player)
	asserts.signal_was_emitted_with_arguments(mission, "mission_initialized", [mission], "Mission init finished")
	asserts.is_not_null(mission.player, "Player found")
	asserts.is_greater_than(mission.stages.size(), 0, "Stages found")
	asserts.is_not_null(mission.current_stage, "Mission has current stage")


func test_mission_stage_setup() -> void:
	describe("Mission stage initialization")

	watch(stage, "stage_started")
	stage._set_active(true)
	asserts.signal_was_emitted(stage, "stage_started", "Stage signal caught")
	asserts.is_equal(mission.current_stage, stage, "Stage is current")
	asserts.is_greater_than(stage.objectives.size(), 0, "Objective found")
	asserts.is_true(stage.active, "Stage is active")


func test_objective_setup() -> void:
	describe("Dummy Objective")
	mission.init_mission(player)

	asserts.is_false(objective.completed, "Objective is not completed")
	asserts.is_false(objective.failed, "Objective is not failed")
	asserts.is_not_null(objective.hud_info, "Objective has hud label")
	objective._set_active(true)
	asserts.is_true(objective.active, "Objective is active")


func test_stage_completion() -> void:
	describe("Stage completion")
	mission.init_mission(player)
	watch(stage, "stage_completed")

	stage._objective_completed(objective)
	asserts.signal_was_emitted_with_arguments(stage, "stage_completed", [stage], "Stage completion signal caught")
	asserts.is_class_instance(mission.current_stage, MissionStageEnd, "Current stage is 'MissionStageEnd'")


func test_stage_failure() -> void:
	describe("Stage failure")
	mission.init_mission(player)
	watch(stage, "stage_failed")

	stage._objective_failed(objective)
	asserts.signal_was_emitted_with_arguments(stage, "stage_failed", [stage], "Stage failure signal caught")
	asserts.is_class_instance(mission.current_stage, MissionStageFail, "Current stage is 'MissionStageFail'")


func test_simple_objective_completion() -> void:
	describe("Objective completion")
	mission.init_mission(player)
	watch(objective, "objective_completed")

	objective._complete_objective()
	_test_objective_completion()


func test_simple_objective_failure() -> void:
	describe("Objective failure")
	mission.init_mission(player)
	watch(objective, "objective_failed")

	objective._fail_objective()
	_test_objective_failure()


func _test_objective_completion() -> void:
	asserts.signal_was_emitted_with_arguments(objective, "objective_completed", [objective], "Objective completion signal caught")
	asserts.is_true(objective.completed, "'objective.completed' is true")
	asserts.is_false(objective.active, "Objective is not active")


func _test_objective_failure() -> void:
	asserts.signal_was_emitted_with_arguments(objective, "objective_failed", [objective], "Objective completion signal caught")
	asserts.is_true(objective.failed, "'objective.failed' is true")
	asserts.is_false(objective.active, "Objective is not active")
