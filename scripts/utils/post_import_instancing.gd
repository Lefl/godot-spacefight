tool
extends EditorScenePostImport


const ORIGINAL_PREFIX = "oi_"
const PLACEHOLDER_PREFIX = "i_"
const POSITION_SUFFIXES = [
	"f", # Front
	"a", # Rear (Aft)
	"l", # Left
	"r", # Right
	"t", # Top
	"b"  # Bottom
	]

var instanceable_nodes := {}
var root_node: Node


func post_import(scene) -> Object:
	root_node = scene
	print("[INFO] Starting post import script...")
	print("Replacing placeholders recursively...")
	var export_directory := Directory.new()
	export_directory.open(get_source_folder())
	var nodes = scene.get_children()
	# Find all instanceable nodes
	instanceable_nodes = find_instanceables(nodes)
	recursive_replace(scene)
	fix_children_owner(scene, scene)

	for i_name in instanceable_nodes:
		if export_directory.file_exists(i_name):
			export_directory.remove(i_name)

	print("[INFO] Finished post processing imported scene.")
	scene.print_tree()
	return scene


func recursive_replace(node: Node) -> void:
	var children: Array = node.get_children()
	if children == null:
		return

	for child in children:
		recursive_replace(child)
		var placeholders := find_placeholders(child.get_children())
		if not placeholders.empty():
			replace_placeholders(placeholders)
		else:
			continue

	var placeholders := find_placeholders(node.get_children())
	replace_placeholders(placeholders)


func find_placeholders(nodes: Array) -> Array:
	var placeholders := []
	for node in nodes:
		var node_name: String = node.name
		if node_name.begins_with(PLACEHOLDER_PREFIX):
			placeholders.append(node)
	return placeholders


func replace_placeholders(placeholders: Array) -> void:
	for ph in placeholders:
		print("[INFO] Replacing %s..." % ph.name)
		var ph_name: String = ph._name.lstrip(PLACEHOLDER_PREFIX)
		# Duplicate the node
		var instanceable: Node = instanceable_nodes[get_instanceable_name(ph_name)]
		# Rename the instance
		# Add it to the tree
		var instance := instanceable.duplicate(Node.DUPLICATE_USE_INSTANCING)
		var tform = ph.transform
		ph.replace_by(instance, true)
		instance.transform = tform
		instance.name = get_real_name(ph_name)


func fix_children_owner(parent_node: Node, owner: Node) -> void:
	var children: Array = parent_node.get_children()
	if children.empty():
		return
	for child in children:
		print("Fixing owner of %s" % child.name)
		child.set_owner(owner)
		fix_children_owner(child, owner)


func find_instanceables(nodes: Array) -> Dictionary:
	var instanceables = {}
	for node in nodes:
		var node_name: String = node.name
		if node_name.begins_with(ORIGINAL_PREFIX):
			instanceables[node_name.lstrip(ORIGINAL_PREFIX)] = node
		else:
			continue
	return instanceables


# Return a name that does not have the prefixes and blenders duplication prefix (e.g.: .001)
func get_real_name(old_name: String) -> String:
	var real_name := old_name
	if old_name[len(old_name) - 1].is_valid_integer():
		real_name = old_name.left(len(old_name) - 3)
	return real_name


# Returns a name that is like the instanceable without Pre- or Suffx
func get_instanceable_name(placeholder_name: String) -> String:
	var instanceable_name := get_real_name(placeholder_name)
	if instanceable_name[len(instanceable_name) - 2] == "_":
		if instanceable_name[len(instanceable_name) - 1] in POSITION_SUFFIXES:
			instanceable_name = instanceable_name.left(len(instanceable_name) - 2)

	return instanceable_name
