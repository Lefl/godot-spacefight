tool
class_name Weapon
extends Node

signal spawn_projectile(source, projectile_id, damage_per_shot, egress_node, target)
enum FiringModes {PRIMARY_FIRE, SECONDARY_FIRE, TERTIARY_FIRE}
export (FiringModes) var firing_mode
export var rate_of_fire := 1.0 setget _rof_recalc
export var damage_per_shot := 1.0 setget _dpsh_recalc
export var damage_per_second := 1.0
export var projectile_name := "spec"
export (Array, NodePath) var egress_node_paths

var source: Spatial
var projectile: Dictionary
var egress_nodes := []
var en_idx := 0
var ready := true
var fire_delta := 1.0/rate_of_fire
var fire_time := 0.0


func _ready() -> void:
	if Engine.editor_hint:
		set_process(false)
		return
	add_to_group("weapons")
	projectile = ResHandler.get_projectile_by_name(projectile_name)
	egress_nodes = _init_egress_node_array()
	if not egress_nodes:
		print("[WARNING] No egress nodes defined in %s" % get_path())
	fire_delta = 1.0/rate_of_fire


func _process(delta) -> void:
	if fire_time < fire_delta:
		ready = false
		fire_time += delta
	else:
		ready = true


master func fire(target: Spatial = null) -> void:
	if fire_time < fire_delta:
		return
	fire_time = 0.0
	ready = false
	spawn_projectile(source, projectile.id, damage_per_shot, _get_next_egress_node(), target)


func spawn_projectile(source: Spatial, projectile_id: int, damage: float, egress_node: Spatial, target: Spatial = null) -> void:
	if get_tree().has_network_peer():
		var source_path: NodePath
		var egress_path: NodePath
		var target_path: NodePath

		source_path = source.get_path()
		egress_path = egress_node.get_path()
		if target:
			target_path = target.get_path()

		rpc("spawn_remote_projectile", source_path, projectile_id, damage, egress_path, target_path)
	else:
# warning-ignore:unsafe_property_access
		emit_signal("spawn_projectile", source, projectile_id, damage, egress_node, target)


puppetsync func spawn_remote_projectile(source_path: NodePath, projectile_id: int, egress_path: NodePath, target_path: NodePath) -> void:
	var r_source = get_node_or_null(source_path)
	var r_egress_node = get_node_or_null(egress_path)
	var r_target = get_node_or_null(target_path)
# warning-ignore:unsafe_property_access
	emit_signal("spawn_projectile", r_source, projectile_id, r_egress_node, r_target)


func _rof_recalc(new_rof: float) -> void:
	self.damage_per_second = damage_per_shot * new_rof
	rate_of_fire = new_rof


func _dpsh_recalc(new_dpsh: float) -> void:
	self.damage_per_second = new_dpsh * rate_of_fire
	damage_per_shot = new_dpsh


func _init_egress_node_array() -> Array:
	if egress_node_paths.size() == 0:
		return []
	var egress_nodes_array := []
	for nodepath in egress_node_paths:
		var node = get_node_or_null(nodepath)
		if node:
			egress_nodes_array += _get_egress_node_array(node)
	return egress_nodes_array


func _get_egress_node_array(egress_node: Node) -> Array:
	var egress_nodes_array := []
	if egress_node is Position3D or egress_node is ActorControlledTurret:
		egress_nodes_array.append(egress_node)
	else:
		for node in egress_node.get_children():
			if node is Position3D or egress_node is ActorControlledTurret:
				egress_nodes_array.append(node)

	return egress_nodes_array


func _get_next_egress_node() -> Position3D:
	en_idx = en_idx + 1 if en_idx < egress_nodes.size() - 1 else 0
	var en: Position3D
	if egress_nodes[en_idx] is ActorControlledTurret:
		if egress_nodes[en_idx].can_fire:
			en = egress_nodes[en_idx].egress_node
		else:
			en = _get_next_egress_node() # TODO: Do you really want to loop forever, forever, FOOOREEEVER??
	else:
		en = egress_nodes[en_idx]
	return en
