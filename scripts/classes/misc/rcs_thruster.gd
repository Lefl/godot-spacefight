class_name RCSThruster
extends Particles
# A simple script for Reaction Control System thrusters.
# https://en.wikipedia.org/wiki/Reaction_control_system
# It calculates the acceleration of the thruster and emits particles if it needs to thrust.
# This implementation could be well improved, currently lateral movement works best.
# Angular movement is a bit finnicky as it is.


var last_local_origin = Vector3()
var old_transform: Transform


func _ready() -> void:
	old_transform = global_transform


func _physics_process(delta) -> void:
	if not visible:
		set_physics_process(false)

	var local_origin := global_transform.origin - old_transform.origin
	var a: Vector3 = (local_origin - last_local_origin) / delta

	if (a.dot(global_transform.basis.z) < -0.0075):
		emitting = true
	else:
		emitting = false

	last_local_origin = local_origin
	old_transform = global_transform
