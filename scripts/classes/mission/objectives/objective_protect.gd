class_name MissionObjectiveProtect
extends MissionObjective


export (NodePath) var objective_path: NodePath
var objective: Actor
var distance: float


func _ready() -> void:
	objective = (get_node(objective_path) as Actor)
	if objective == null:
		print(
			"[ERROR] Objective is null in %s in parent %s, game will crash on activation"
			 % [name, get_parent().name]
			)
		return
	objective.connect("has_died", self, "_objective_died")
	_set_progress_string(objective)


func _process(_delta) -> void:
	_set_progress_string(objective)
	update_hud_info()


func _set_active(val: bool) -> void:
	._set_active(val)
	if val:
		objective.add_to_group("objective")
		objective.add_to_group("target_of_interest")
	else:
		objective.remove_from_group("objective")
	active = val


func _objective_recieved_damage(_actor: Actor) -> void:
	_set_progress_string(objective)
	update_hud_info()


func _objective_died(_actor: Actor) -> void:
	_fail_objective()


func _set_progress_string(actor: Actor) -> void:
	progress_string = "H: %s/%s" % [int(actor.health), actor.MAX_HEALTH]
	if actor.MAX_SHIELDS > 0:
		progress_string += " S: %s/%s" % [int(actor.shields), actor.MAX_SHIELDS]
