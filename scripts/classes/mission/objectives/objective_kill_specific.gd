class_name MissionObjectiveKillSpecific
extends MissionObjective


export (NodePath) var objective_path: String
var objective: Actor


func _ready() -> void:
	objective = (get_node_or_null(objective_path) as Actor)
	objective.connect("has_died", self, "_objective_died")


func set_active(val: bool) -> void:
	._set_active(val)
	objective.add_to_group("objective")
	active = val


func _objective_died(_actor: Actor) -> void:
	_complete_objective()
