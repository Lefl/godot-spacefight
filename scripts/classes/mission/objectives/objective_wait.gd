extends MissionObjective
class_name MissionObjectiveWait

export (float) var WAIT_TIME := 0.0
var wait_timer := WAIT_TIME


func _ready() -> void:
	wait_timer = WAIT_TIME


func _process(delta) -> void:
	wait_timer -= delta
	if wait_timer <= 0.0:
		progress_string = "00:00"
		_complete_objective()
		return

	progress_string = _time_repr(wait_timer)
	update_hud_info()
	return


func _time_repr(seconds: float) -> String:
	var stime: String

	var iminutes := int(seconds / 60)
	if iminutes / 10.0 < 1.0:
		stime = "0%s" % iminutes
	else:
		stime = str(iminutes)

	stime += ":"

	var iseconds := int(fmod(seconds, 60.0))
	if iseconds / 10.0 < 1.0:
		stime += "0%s" % iseconds
	else:
		stime += str(iseconds)
	return stime
