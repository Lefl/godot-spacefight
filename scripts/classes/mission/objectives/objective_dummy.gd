class_name MissionObjectiveDummy
extends MissionObjective


func _input(event) -> void:
	if event is InputEventKey:
		if event.scancode == KEY_P:
			_fail_objective()
		if event.scancode == KEY_O:
			_complete_objective()
