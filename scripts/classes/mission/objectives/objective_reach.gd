extends MissionObjective
class_name MissionObjectiveReach

export (NodePath) var objective_path: NodePath
var player: PlayerShip
export (float) var margin := 0.0
var objective: Spatial


func _ready() -> void:
	objective = get_node(objective_path)
	if not objective:
		print("[WARNING] %s in parent %s has no valid objective assigned to it" % [self.name, get_parent().name])


# The player is not available on _ready()
# Get it when the objective is activated
func _set_active(val: bool) -> void:
	if val:
		if MultiplayerHandler.is_singleplayer() or not is_network_master():
# warning-ignore:unsafe_property_access
			player = get_parent().get_parent().player
		objective.add_to_group("objective")
	else:
		objective.remove_from_group("objective")

	._set_active(val)
	active = val


func _process(_delta) -> void:
	if not MultiplayerHandler.is_singleplayer() and is_network_master():
		_check_multiple_distances()
	else:
		_check_distance()


func _check_multiple_distances() -> void:
	var players_at_objective := 0
	var objective_position := objective.global_transform.origin
# warning-ignore:unsafe_property_access
	for p in get_parent().get_parent().players_alive:
		var distance := 0.0
		distance = objective_position.distance_to(p.global_transform.origin)
		distance -= margin
		if distance <= 0:
			players_at_objective += 1
# warning-ignore:unsafe_property_access
	if players_at_objective == get_parent().get_parent().players_alive.size():
		_complete_objective()


func _check_distance() -> void:
	var distance := objective.global_transform.origin.distance_to(player.global_transform.origin)
	distance -= margin
	progress_string = "%sm" % stepify(distance, 0.01)
	if distance <= 0:
		progress_string = "0.00m"
		_complete_objective()
		return
	update_hud_info()
