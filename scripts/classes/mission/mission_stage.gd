class_name MissionStage, "res://icons/mission.svg"
extends Node


signal stage_started
signal stage_completed
signal stage_failed

puppet var active := false setget _set_active
var animation_players := []
puppet var objectives_completed: int setget _set_objectives_completed
onready var objectives := get_children()


func _ready() -> void:
	# Call _on_ready to allow end and fail stages to fully overwrite ready.
	_on_ready()


func _on_ready() -> void:
	for objective in objectives:
		if objective is MissionAnimationPlayer:
			animation_players.append(objective)
			objectives.erase(objective)
		else:
			objective.connect("objective_completed", self, "_objective_completed", [], CONNECT_ONESHOT)
			objective.connect("objective_failed", self, "_objective_failed", [], CONNECT_ONESHOT)


func _set_active(val: bool) -> void:
	if val:
		print("[INFO] Mission activated Stage %s" % name)
		for player in animation_players:
			if val and player.start_mode == player.START_MODES.ON_ACTIVATION:
				player._play()
				if player.is_transition:
					yield(player, "animation_finished")
			elif not val and player.start_mode == player.START_MODES.ON_DEACTIVATION:
				player._play()

	for objective in objectives:
		objective.active = true
		if objective is MissionObjectiveProtect:
			objective.completed = true
			objectives_completed += 1
		emit_signal("stage_started")

	active = val


func _objective_completed(_objective: MissionObjective) -> void:
	if MultiplayerHandler.is_singleplayer():
		self.objectives_completed += 1
	elif is_network_master():
		self.objectives_completed += 1
		rset("objectives_completed", objectives_completed)


func _objective_failed(_objective: MissionObjective) -> void:
	if MultiplayerHandler.is_singleplayer() or is_network_master():
		_fail_stage()
	if is_network_master():
		rpc("_fail_stage")


func _fail_stage() -> void:
		emit_signal("stage_failed", self)
		active = false


func _set_objectives_completed(amount: int) -> void:
	if amount == objectives.size():
		emit_signal("stage_completed", self)
	objectives_completed = amount
