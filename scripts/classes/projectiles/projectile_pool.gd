class_name ProjectilePool
extends Spatial

var projectiles := {}


func _ready() -> void:
	call_deferred("connect_signals")


func activate_projectile(source: Actor, id: int, damage: float, egress_node: Spatial, target: Spatial):

	var projectile: Projectile

	# Find a deactivated projectile that we can reuse.
	projectile = get_inactive_projectile(id)

	# If there's no projectile we can use, make a new one.
	if not projectile:
		projectile = _new_projectile(id)
	# Set parameters

	projectile.source = source

	projectile.damage = projectile.damage.duplicate()
	projectile.damage["damage"] = damage

	projectile.global_transform = egress_node.global_transform
	if projectile is ProjectileHoming:
		(projectile as ProjectileHoming).target = target
	projectile.egress_node = egress_node
	projectile.active = true


func _new_projectile(id: int) -> Projectile:
	var projectile_properties = ResHandler.get_projectile_by_id(id)
	var projectile: Projectile

	match projectile_properties["type"]:
		Projectile.Type.PROJECTILE:
			projectile = Projectile.new(projectile_properties, self)
		Projectile.Type.BEAM:
			projectile = ProjectileBeam.new(projectile_properties, self)
		Projectile.Type.HOMING:
			projectile = ProjectileHoming.new(projectile_properties, self)

	add_child(projectile)
	projectile.connect("active_changed", self, "_projectile_active_changed")
	return projectile


func get_inactive_projectile(id: int) -> Projectile:
	if projectiles.has(id):
		if projectiles[id].size() > 0:
			return projectiles[id][0]
		else:
			return null
	else:
		projectiles[id] = []
	return null


func _projectile_active_changed(projectile: Projectile, status: bool) -> void:
	if status:
		projectiles[projectile.id].erase(projectile)
	else:
		projectiles[projectile.id].append(projectile)


func connect_signals() -> void:
	for node in get_tree().get_nodes_in_group("weapons"):
		node.connect("spawn_projectile", self, "activate_projectile")
