extends Spatial
class_name ActorControlledTurret

enum {PART_HEAD, PART_BARREL}

export (float) var MAX_PITCH: float
export (float) var MIN_PITCH: float
export (float) var PITCH_MARGIN: float
export var TURN_SPEED = 0.075
export (NodePath) var head_node_path
export (NodePath) var barrel_node_path
export (NodePath) var egress_node_path

var head_node: Spatial
var barrel_node: Spatial
var egress_node: Position3D
var can_fire := true


func _ready() -> void:
	head_node = get_node(head_node_path)
	barrel_node = get_node(barrel_node_path)
	egress_node = get_node(egress_node_path)


puppet func aim_at(aim_position) -> void:
	var pos: Vector3 = aim_position
	if not pos:
		pos = Vector3()
	var n_head_tform := head_looking_at(pos)
	var n_barrel_tform := barrel_looking_at(pos)
	turn_part(PART_HEAD, n_head_tform)
	turn_part(PART_BARREL, n_barrel_tform)


func turn_part(part: int, new_tform: Transform) -> void:
	var current_tform: Transform
	if part == PART_HEAD:
		current_tform = head_node.global_transform # Get the heads current transform
	elif part == PART_BARREL:
		current_tform = barrel_node.transform

	# Reset transform to have a scale of 1, to prevent interpolating the scale.
	current_tform = current_tform.orthonormalized()
	new_tform = current_tform.interpolate_with(new_tform, TURN_SPEED)

	if part == PART_HEAD:
		head_node.global_transform = new_tform
	elif part == PART_BARREL:
		barrel_node.transform = new_tform


func head_looking_at(position: Vector3) -> Transform:
	# Rotate the Turret base transform to face the position.
	var head_tform := head_node.global_transform

	var global_target_pos := position #Get the position of the nearest enemy.
	var target_pos := global_target_pos# - head_node.get_global_transform().origin

	# Project our target to a plane
	var po := head_tform.origin
	var px := po + head_tform.basis.x
	var pz := po + head_tform.basis.z
	var plane := Plane(po, px, pz)

	target_pos = plane.project(target_pos)
	head_tform = head_tform.looking_at(target_pos, head_tform.basis.y)

	return head_tform.orthonormalized() # return the Transform for the turret base.



func barrel_looking_at(position: Vector3) -> Transform:

	var barrel_global_tform := barrel_node.global_transform
	var barrel_tform := barrel_node.transform
	var head_tform := head_node.global_transform

	var target_global_pos := position #Get the position of the nearest enemy.
	var target_pos := target_global_pos - barrel_global_tform.origin# + barrel_tform.origin#target_global_pos - barrel_global_tform.origin


	var hpo := Vector3(0, 0, 0)
	var hpx := hpo + head_tform.basis.x.normalized()
	var hpz := hpo + head_tform.basis.z.normalized()
	var plane := Plane(hpo, hpx, hpz) # Create a horizontal plane as seen from the head.

	#Check if the target is above or below the controller this is used further down below.
	var barrel_sign: int = 0
	if plane.is_point_over(target_pos):
		barrel_sign = 1
	else:
		barrel_sign = -1

	var target_projected_pos := plane.project(target_pos) # Project the enemys position to the plane
	var target_dir := (target_projected_pos - barrel_tform.origin).normalized()
	var barrel_angle := target_dir.angle_to((target_pos - barrel_tform.origin).normalized()) * barrel_sign

	if barrel_angle > deg2rad(MAX_PITCH):
		if barrel_angle > deg2rad(MAX_PITCH) * PITCH_MARGIN:
			can_fire = false
		barrel_angle = deg2rad(MAX_PITCH)
	elif barrel_angle < deg2rad(MIN_PITCH):
		if barrel_angle < deg2rad(MIN_PITCH) * PITCH_MARGIN:
			can_fire = false
		barrel_angle = deg2rad(MIN_PITCH)
	else:
		can_fire = true

	var barrel_quat = Quat(Vector3(1, 0, 0), barrel_angle)
	barrel_tform = Transform(Basis(barrel_quat), barrel_tform.origin)

	# Check if the angle is below/above the min/max angle.
	return barrel_tform
