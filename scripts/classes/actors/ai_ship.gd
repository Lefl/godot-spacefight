class_name AIShip
extends AIActor


export var turn_rate := 0.5

var vel_mult := 0.0
var strafe_vel_mult := 0.0


func _ready() -> void:
	randomize()


func move_to(position: Vector3, collision_points: Dictionary, delta: float) -> KinematicCollision:
	# This needs smoother acceleration and decceleration!

	var evade_position := calculate_evade_position(position, collision_points)
	var norm_evade_position := evade_position.normalized()
	if norm_evade_position != Vector3.ZERO:
		var tform := global_transform.looking_at(global_transform.origin + evade_position, norm_evade_position.cross(-global_transform.basis.x).normalized())
		var angle := tform.basis.z.angle_to(global_transform.basis.z) / PI
		global_transform.basis = global_transform.basis.orthonormalized().slerp(tform.basis.orthonormalized(), ease_rotation(1.0 - angle * 0.125) * (turn_rate * PI) * delta)
	var d := position - global_transform.origin

	var new_vel_mult = 0.01*d.length_squared() + 0.5
	vel_mult = vel_mult + delta if new_vel_mult > vel_mult else new_vel_mult
	velocity = -global_transform.basis.z * min(vel_mult * MAX_VELOCITY, MAX_VELOCITY) * delta
	if velocity.length_squared() > d.length_squared():
		velocity = d
	# Add strafe velocity, calculate the z axis out of the vector to the target position
	#var plane := Plane(-global_transform.basis.orthonormalized().z.normalized(), 0.0)
	var d_strafe = d - (d * -global_transform.basis.z)
	var new_strafe_vel_mult = 0.01*d_strafe.length_squared() + 0.5
	strafe_vel_mult = strafe_vel_mult + delta if new_strafe_vel_mult > strafe_vel_mult else new_strafe_vel_mult
	var strafe_velocity = d_strafe.normalized() * min(strafe_vel_mult * MAX_STRAFE_VELOCITY, MAX_STRAFE_VELOCITY) * delta
	if strafe_velocity.length_squared() > d_strafe.length_squared():
		strafe_velocity = d_strafe
	velocity += strafe_velocity

	var collision := move_and_collide(velocity)
	if collision_points["target"].empty():
		return collision
	else:
		return move_and_collide(target.global_transform.origin - global_transform.origin, true, true, true)


func get_raycast_collisions() -> Dictionary:
	var state := get_world().direct_space_state
	var collisions := []
	var occupied_points := PoolVector3Array()
	var target_points := PoolVector3Array()
	var free_points := PoolVector3Array()
	for x in range(-6, 7, 1):
		for y in range(-6, 7, 1):
			var from := global_transform.origin + global_transform.basis.x * x + global_transform.basis.y * y
			var to := from - global_transform.basis.z * 50
			var collision := state.intersect_ray(from, to, [self])
			if not collision.empty():
				if collision.collider == target:
					target_points.append(collision.position)
				else:
					occupied_points.append(collision.position)
			else:
				free_points.append(to)
	return {"free": free_points, "occupied": occupied_points, "target": target_points}


func calculate_evade_position(target_position: Vector3, points: Dictionary) -> Vector3:
	var middle_ray := global_transform.origin - global_transform.basis.z * 50
	var evade_position := (target_position - global_transform.origin).normalized()
	if points.occupied.empty():
		return evade_position
	for point in points.free:
		evade_position += (point - middle_ray).normalized()
	for point in points.occupied:
		var d: Vector3 = point - middle_ray
		d -= d * -global_transform.basis.z
		evade_position -= (point - middle_ray).normalized() * (max(-d.length()/8.49, 0) + 0.1)
	return evade_position


func ease_rotation(x: float) -> float:
	# easeInOutSine
	# the 1.1 and 2.1 are to avoid the AI locking up and flying in a straight line
	return -(cos(PI * x) -1.1) / 2.1
