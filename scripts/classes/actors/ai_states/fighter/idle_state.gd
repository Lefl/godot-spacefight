extends AIState


"""
	State for roaming around in 3D space until a target is found
"""


export var max_roam_from_origin := 100
export var roam_distance := 100.0
var roam_position: Vector3
var path: Curve3D


func _enter(_previous_State: AIState) -> void:
	roam_position = _get_random_position()
	parent.aim_turrets(roam_position)


func _physics_process(delta: float) -> void:
	var new_target = parent.acquire_target()
	if new_target:
		parent.target = new_target
		parent.state = $"../HuntState"
		return

	if parent.global_transform.origin.is_equal_approx(roam_position):
		parent.state = self
	else:
		var ray_collisions: Dictionary = parent.get_raycast_collisions()
		parent.move_to(roam_position, ray_collisions, delta)


func _get_random_position() -> Vector3:
	var direction := Vector3(rand_range(-1.0, 1.0), rand_range(-1.0, 1.0), rand_range(-1.0, 1.0)).normalized()
	var direction_to_origin: Vector3 = -parent.global_transform.origin.normalized()
	var roam_factor: float = parent.global_transform.origin.distance_to(Vector3.ZERO) / max_roam_from_origin
	return parent.global_transform.origin + direction.linear_interpolate(direction_to_origin, roam_factor) * roam_distance
