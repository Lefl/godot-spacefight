extends AIState


"""
	Follow a target
"""

var follow_offset := 15.0


func _enter(_previous_State: AIState) -> void:
	pass


func _physics_process(delta: float) -> void:
	var target_position: Vector3 = parent.calculate_preaim_pos(parent.target, parent.MAX_VELOCITY, -parent.target.get_linear_velocity() * follow_offset)
	var ray_collisions: Dictionary = parent.get_raycast_collisions()
	parent.move_to(target_position, ray_collisions, delta)

	parent.aim_turrets(parent.target.global_transform.origin)
	parent.get_active_weapon(Weapon.FiringModes.PRIMARY_FIRE).fire(parent.target)

	if not ray_collisions["target"].empty():
		parent.state = $"../RunState"


func _on_target_locked(_target: Actor) -> void:
	pass


func _on_target_unlocked(_target: Actor) -> void:
	pass


func _on_damage_received(_damage: Dictionary, _source) -> void:
	pass


func _exit() -> void:
	pass
