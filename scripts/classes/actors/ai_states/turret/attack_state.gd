extends AIState


"""
	State for attacking a target
"""


func _enter(_previous_State: AIState) -> void:
	pass


func _physics_process(_delta: float) -> void:
	parent.fire_at_target()
	if not parent.is_target_valid(parent.target):
		parent.state = $"../IdleState"

