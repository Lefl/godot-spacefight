class_name Protege
extends AIActor


signal path_finished
enum movement_types {ANIMATION, PATH, STATIC}

export (movement_types) var movement_type: int
export (bool) var moving := false
export (NodePath) var path_nodepath: NodePath
export var angular_velocity := PI/2

var dist_travelled := 0.0
var reverse_curve := false
var curve: Curve3D
var points := []
var curve_length: float
var last_pos: Vector3
var linear_velocity := 10.0


func _ready() -> void:
	last_pos = global_transform.origin

	if movement_type == movement_types.PATH:
		init_path()


func init_path() -> void:
	if path_nodepath.is_empty():
		print("[WARNING] %s is supposed to follow a path, but no path was provided!" % name)
		return
	curve = (get_node(path_nodepath) as Path).get_curve()
	points = curve.get_baked_points()
	if reverse_curve == true:
		points.invert()

	curve_length = curve.get_baked_length()
	global_transform.origin = points[0]

	last_pos = global_transform.origin
	moving = true


func _process(delta) -> void:
	if moving and (MultiplayerHandler.is_singleplayer() or get_tree().is_network_server()):
		if movement_type == movement_types.PATH and dist_travelled < curve_length:
			follow_path(delta)
		align_to_movement(delta)

		if not MultiplayerHandler.is_singleplayer() and get_tree().is_network_server():
			rpc_unreliable("set_puppet_transform_velocity", global_transform, Vector3())

		if dist_travelled >= curve_length:
			emit_signal("path_finished")


func follow_path(delta) -> void:
	last_pos = global_transform.origin
	var n_pos = curve.interpolate_baked(dist_travelled, true)
	dist_travelled += velocity*delta
	global_transform.origin = n_pos
	velocity = n_pos - last_pos


func align_to_movement(delta) -> void:
	var tform = global_transform
	var next_pos = curve.interpolate_baked(dist_travelled + velocity*delta , true)
	if tform.origin != last_pos:
		var rot_per_frame = angular_velocity*delta
		var dir_to_last_pos = (last_pos - tform.origin).normalized()
		var angle = next_pos.normalized().angle_to(dir_to_last_pos)

		var p = 0
		if angle > 0:
			p = rot_per_frame/angle

		var n_tform = tform
		n_tform.origin = next_pos
		n_tform = n_tform.looking_at(tform.origin, curve.interpolate_baked_up_vector(dist_travelled + velocity*delta))
		n_tform = tform.interpolate_with(n_tform, p)

		global_transform = n_tform


func set_position(position) -> void:
	var t = global_transform
	t.origin = position
	global_transform = t
