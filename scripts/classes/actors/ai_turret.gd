extends AIActor
class_name AITurret
# This class is used by all turrets.
# If a turret is a beam turret or a normal projectile should be determined by the projectile and not by the turret.
# This will not be a problem as a beam turret also might use animation and can use multiple barrels.
# The firing of the turrets is handled by an animation.

enum {PART_HEAD, PART_BARREL}
export (int) var MAX_PITCH: int
export (int) var MIN_PITCH: int
export var TURN_SPEED := 0.075
export (NodePath) var animation_player_path: NodePath
export (String) var animation_name: String
export (NodePath) var head_node_path
export (NodePath) var barrel_node_path

var animation_player: AnimationPlayer
var head_node: Spatial
var barrel_node: Spatial
var head_rest_tform: Transform
var barrel_rest_tform: Transform
var firing := false


func _ready() -> void:
	head_node = get_node(head_node_path)
	barrel_node = get_node(barrel_node_path)

	head_rest_tform = head_node.global_transform
	barrel_rest_tform = barrel_node.transform

	animation_player = (get_node(animation_player_path) as AnimationPlayer)

	if get_active_weapon(Weapon.FiringModes.PRIMARY_FIRE).projectile.id == Projectile.Type.BEAM:
		MAX_TARGET_DIST = get_active_weapon(Weapon.FiringModes.PRIMARY_FIRE).projectile.max_distance


func fire_at_target() -> bool:
	# Returns true if firing is successful and false if it can not aim at the target.
	var target_position := target.global_transform.origin
	if get_active_weapon(Weapon.FiringModes.PRIMARY_FIRE).projectile.linear_velocity:
		target_position = calculate_preaim_pos(
			target, get_active_weapon(Weapon.FiringModes.PRIMARY_FIRE).projectile.linear_velocity
			)

	var n_head_tform = head_looking_at_target(target)
	var n_barrel_tform = barrel_looking_at_target(target)
	if n_barrel_tform == Transform():
		firing = false
		return false
	var target_dir = (target_position - barrel_node.global_transform.origin).normalized()
	var barrel_dir = -barrel_node.global_transform.basis.z.normalized()
	var angle_to_target = barrel_dir.angle_to(target_dir)
	if angle_to_target <= 0.0475:
		fire()
	else:
		firing = false
	turn_part(PART_HEAD, n_head_tform)
	turn_part(PART_BARREL, n_barrel_tform)
	return true


func turn_to_default_transform() -> void:
	# Get the transformation of the whole turret and use it as a basis for the head transformation
	var head_tform = global_transform
	head_tform.origin = head_node.global_transform.origin

	turn_part(PART_HEAD, head_tform)
	turn_part(PART_BARREL, barrel_rest_tform)
	firing = false


func turn_part(part: int, new_tform: Transform) -> void:
	var current_tform
	if part == PART_HEAD:
		current_tform = head_node.global_transform # Get the heads current transform
	elif part == PART_BARREL:
		current_tform = barrel_node.transform

	# Reset transform to have a scale of 1, to prevent interpolating the scale.
	current_tform = current_tform.orthonormalized()
	new_tform = current_tform.interpolate_with(new_tform, TURN_SPEED)

	if part == PART_HEAD:
		new_tform.basis = new_tform.basis.scaled(head_rest_tform.basis.get_scale()) # Reapply scale
		head_node.global_transform = new_tform
	elif part == PART_BARREL:
		barrel_node.transform = new_tform


func fire() -> void:
	if not firing:
		firing = true
		animation_player.play(animation_name)


func _animation_fire() -> void:
	if !get_tree().has_network_peer() or is_network_master():
		get_active_weapon(Weapon.FiringModes.PRIMARY_FIRE).fire(target)


func head_looking_at_target(entity: Spatial) -> Transform:
	# We use local Transforms here because we want to avoid scaling issues when applying it.
	# Rotate the Turret base transform to face the enemy.
	var head_tform = head_node.global_transform
	#Get the position of the nearest enemy.
	var target_position := entity.global_transform.origin
	target_position = calculate_preaim_pos(
		target, get_active_weapon(Weapon.FiringModes.PRIMARY_FIRE).projectile.linear_velocity
		)

	# Project our target to a plane
	var po = head_tform.origin
	var px = po + head_tform.basis.x
	var pz = po + head_tform.basis.z
	var plane = Plane(po, px, pz)

	target_position = plane.project(target_position)
	head_tform = head_tform.looking_at(target_position, head_tform.basis.y)

	return head_tform.orthonormalized() # return the Transform for the turret base.


func barrel_looking_at_target(entity: Spatial) -> Transform:
	var barrel_position := barrel_node.global_transform.origin
	var barrel_tform := barrel_node.transform
	var head_tform: Transform = head_node.global_transform

	# Get the local position of the nearest enemy.
	var target_global_pos := entity.global_transform.origin
	target_global_pos = calculate_preaim_pos(
		target, get_active_weapon(Weapon.FiringModes.PRIMARY_FIRE).projectile.linear_velocity
		)
	var target_pos := target_global_pos - barrel_position

	# Create an horizontal plane as seen from the head.
	var hpo := Vector3(0, 0, 0)
	var hpx := hpo + head_tform.basis.x.normalized()
	var hpz := hpo + head_tform.basis.z.normalized()
	var plane := Plane(hpo, hpx, hpz)

	# Check if the target is above or below this plane.
	var barrel_sign := 0
	if plane.is_point_over(target_pos):
		barrel_sign = 1
	else:
		barrel_sign = -1

	# Project the enemys position to the plane
	# This is as if the target is local Z = 0.
	var target_projected_pos := plane.project(target_pos)
	# Now get the direction to the projected point.
	var target_projected_direction := (target_projected_pos - barrel_tform.origin).normalized()
	var target_direction := (target_pos - barrel_tform.origin).normalized()
	# Now get the angle between the flat Z target and the real target
	var barrel_angle := target_projected_direction.angle_to(target_direction)
	# Reapply the sign
	barrel_angle *= barrel_sign
	# Construct a Transform.
	var barrel_quat := Quat(Vector3(1, 0, 0), barrel_angle)
	var new_barrel_tform := Transform(Basis(barrel_quat), barrel_tform.origin)

	# Check if the angle is below/above the min/max angle.
	if barrel_angle < deg2rad(MAX_PITCH) and barrel_angle > deg2rad(MIN_PITCH):
		return new_barrel_tform
	else:
		return Transform()


func is_target_valid(target) -> bool:
	var is_valid := .is_target_valid(target)
	if is_valid:
		is_valid = can_hit_target(target)
	return is_valid



func can_hit_target(target) -> bool:
	var r := cast_ray(barrel_node.global_transform.origin, target.global_transform.origin)
	if not r.empty() and r.collider == target:
		return true
	else:
		return false


func _on_animation_finished(_last_animation: String) -> void:
	# Restart firing animation if we're firing at something and the animations runs out
	if firing:
		animation_player.play(animation_name)


func die() -> void:
	.die()
	animation_player.stop()
	($AudioStreamPlayer3D as AudioStreamPlayer3D).stop()
