class_name PlayerShip
extends Actor


var hud: ShipHUD
var mission_hud: Control
var rel_mouse_node: Control
var window_res: Vector2 = OS.get_window_size()
onready var camera := ($CameraPivot/Camera as Camera)
onready var camera_pivot := ($CameraPivot as Spatial)

#### Movement Block starting ####
const ACCEL = 2
const BOOST_ACCEL = 3
const DEACCEL = 0.7
const BOOST_MAG = 1.25
const TURN_SPEED = 0.075
const ROTATE_SPEED = {"yp": 1.75, "roll": 0.025}
const ROLL_ACCEL = 6.5
const ROLL_DECCEL = 2.5

var movement_blocked := false
var mouse_ignored := false
var strafe_velocity := Vector3()
remote var is_boosting = false
# These have to be globals for multiplayer
remote var dir := Vector3()
remote var st_dir := Vector3()
remote var roll_dir: int = -1
var roll_speed := 0.0

remote var pitch := 0.0
remote var yaw := 0.0
var mouse_sensitivity := 1.0
#### Movement Block ending ####

#### Health Block starting
var god = false
#### Health Block ending ####

## Turret config
const AIM_POS_CHECK_DISTANCE := 1000



## Markers HUD
var is_markers_enabled := true
var markers := {}


func _ready() -> void:
	if not str(MultiplayerHandler.nuid) == name:
		get_tree().connect("network_peer_disconnected", self, "player_disconnected")
	if is_local_player():
		call_deferred("_setup_hud")

	# Set Camera to not move with the player.
	# This is needed for the to rotate to the camera
	camera_pivot.set_as_toplevel(true)
	# Signal for when window size has changed is used to keep mouse working.
	get_tree().get_root().connect("size_changed", self, "_on_window_size_change")


	if not is_local_player():
		call_deferred("set_process_input", false)


func _setup_hud() -> void:
	print("[INFO] Setting up hud")
	hud = (load("res://scenes/gui/ship_hud/hud.tscn") as PackedScene).instance()
	add_child(hud)
	hud.get_node("Markers").connect("targeted_node", self, "_hud_targeted_node")
	mission_hud = hud.get_node("MissionHud")
	rel_mouse_node = hud.get_node("Reticle")

	#rel_mouse_node.connect("area_entered", self, "reticle_entered_target")


puppet func set_puppet_transforms(
			global_tform: Transform,
			vel: Vector3,
			stvel: Vector3,
			camera_basis: Basis
			) -> void:
	if is_processing() and not movement_blocked:
		.set_puppet_transform_velocity(global_tform, vel)
		(camera_pivot as Spatial).global_transform = Transform(camera_basis, global_tform.origin)
		strafe_velocity = stvel


func _physics_process(delta) -> void:
	# If we are either in singleplayer or are the network master, update our position, etc.
	# This is really shitty for singleplayer BTW.
	if is_local_player():
		try_rotate()
		if Input.is_action_pressed("fire"):
			if !get_tree().has_network_peer():
				get_active_weapon(Weapon.FiringModes.PRIMARY_FIRE).fire(target)
			elif is_local_player():
				get_active_weapon(Weapon.FiringModes.PRIMARY_FIRE).rpc_unreliable("fire", target)

	move(delta)

	if MultiplayerHandler.is_singleplayer() or is_network_master():
		rotate_camera(delta)
		rotate_player()
		if not MultiplayerHandler.is_singleplayer():
			# Set Transforms of the slave, add velocity to it and set the camera rotation.
			rpc_unreliable(
				"set_puppet_transforms",
				global_transform,
				velocity,
				strafe_velocity,
				camera_pivot.global_transform.basis
				)


func _process(_delta) -> void:
	if is_local_player():
		if not target == null and not is_instance_valid(target):
			target = null
		update_reticle()
	aim_turrets(get_aim_pos(AIM_POS_CHECK_DISTANCE))

func clear_markers() -> void:
	for sprite in markers.values():
		sprite.queue_free()
	markers = {}


func disable_markers() -> void:
	clear_markers()
	is_markers_enabled = false


func enable_markers() -> void:
	is_markers_enabled = true


func update_reticle() -> void:
	var r_pos := window_res/2 + Vector2(-yaw, pitch)
	rel_mouse_node.rect_position = r_pos


func move(delta) -> void:
	var tform := transform
	if is_local_player():
		dir = Vector3()
		st_dir = Vector3()

		if not movement_blocked:
			if Input.is_action_pressed("accelerate"):
				dir += tform.basis.z
			if Input.is_action_pressed("decelerate"):
				dir -= tform.basis.z

			# Strafing, uses different velocity
			# This makes it possible to have different speeds for strafing and accelerating.
			if Input.is_action_pressed("strafe_up"):
				st_dir += tform.basis.y
			if Input.is_action_pressed("strafe_down"):
				st_dir -= tform.basis.y

			if Input.is_action_pressed("strafe_left"):
				st_dir += tform.basis.x
			if Input.is_action_pressed("strafe_right"):
				st_dir -= tform.basis.x

			is_boosting = false
			if Input.is_action_pressed("run"):
				is_boosting = true

		# Send the input to the server if we are in multiplayer.
		# Only transmitt if the state has changed.
		if not MultiplayerHandler.is_singleplayer():
			rset_unreliable_id(1, "dir", dir)
			rset_unreliable_id(1, "st_dir", st_dir)
			rset_unreliable_id(1, "is_boosting", is_boosting)
			return

	# Normalize both directional vectors
	dir = dir.normalized()
	st_dir = st_dir.normalized()
	# Get the speed vectors
	var target_vel := dir * MAX_VELOCITY
	var st_target_vel := st_dir * MAX_STRAFE_VELOCITY
	# If we're in newtonian flight mode, cap the velocity vectors.


	var accel: float = 0.0
	var st_accel: float = 0.0
	# If we're accelerating / deccelerating, set the accel vars accordingly
	if dir != Vector3():
		if is_boosting:
			accel = BOOST_ACCEL
		else:
			accel = ACCEL
	else:
		accel = DEACCEL
	if st_dir != Vector3():
		st_accel = ACCEL
	else:
		st_accel = DEACCEL

	if is_boosting:
		# Get the forward target speed, get a percent of it and add it onto the target.
		var target_modifier := (target_vel * get_transform().basis.z.abs()) * BOOST_MAG
		target_vel += target_modifier

	# Interpolate the velocity vectors towards the target vectors
	velocity = velocity.linear_interpolate(target_vel, accel * delta)
	strafe_velocity = strafe_velocity.linear_interpolate(st_target_vel, st_accel * delta)
	# Move with the velocity vectors.
	velocity = move_and_slide(velocity, Vector3(0, 1, 0))
	strafe_velocity = move_and_slide(strafe_velocity, Vector3(0, 1, 0))



func try_rotate() -> void:
	if not mouse_ignored:
		# We only need to send a number, because rolling is binary.

		var new_roll_dir: int = -1
		if Input.is_action_pressed("roll_left"):
			new_roll_dir = 0
		elif Input.is_action_pressed("roll_right"):
			new_roll_dir = 1

		if not MultiplayerHandler.is_singleplayer():
			if new_roll_dir != roll_dir:
				rset_unreliable_id(1, "roll_dir", new_roll_dir)
			rset_unreliable_id(1, "yaw", yaw)
			rset_unreliable_id(1, "pitch", pitch)

		roll_dir = new_roll_dir



func rotate_camera(delta) -> void:
	# Rotate by players mouse and keyboard input. Yaw and pitch are updated in the _input() function.
	var tform: Transform = camera_pivot.global_transform
	tform.origin = global_transform.origin
	var n_tform := tform

	# Get the percentage in the screen
	var cl_yaw := clamp((yaw * 2 / window_res.x), -1, 1)
	var cl_pitch := clamp((pitch * 2 / window_res.y), -1, 1)

	cl_yaw *= ROTATE_SPEED["yp"]
	cl_pitch *= ROTATE_SPEED["yp"]

	# Roll
	# TODO Interpolate this.
	if not mouse_ignored:
		if roll_dir == 0:
			roll_speed = lerp(roll_speed, -ROTATE_SPEED["roll"], delta * ROLL_ACCEL)
		elif roll_dir == 1:
			roll_speed = lerp(roll_speed, ROTATE_SPEED["roll"], delta * ROLL_ACCEL)
		else:
			# The roll_speed + 1 / 1 is obviously there to prevent division by zero
			roll_speed = lerp(roll_speed, 0, delta * ROLL_DECCEL  * (1 + (roll_speed + 1) / 1 * 0.5))
		n_tform.basis = n_tform.basis.rotated(n_tform.basis.z.normalized(), roll_speed)
		#roll_dir = -1
		# Yaw / Pitch
		n_tform.basis = n_tform.basis.rotated(n_tform.basis.y.normalized(), cl_yaw * delta)
		n_tform.basis = n_tform.basis.rotated(n_tform.basis.x.normalized(), cl_pitch * delta)

	camera_pivot.global_transform = n_tform


func rotate_player() -> void:
	# Rotate by players mouse and keyboard input. Yaw and pitch are updated in the _input() function.
	var tform := global_transform
	var n_tform := tform
	n_tform.basis = camera_pivot.global_transform.basis
	n_tform = tform.interpolate_with(n_tform, TURN_SPEED)
	global_transform = n_tform


func recieve_damage(dmg_dict: Dictionary, source: Spatial) -> void:
	if god:
		return
	.recieve_damage(dmg_dict, source)
	update_health_bars()


# Proxy function to update health bars and stuff
puppet func update_health(new_health: float, new_shields: float) -> void:
	.update_health(new_health, new_shields)
	update_health_bars()


func update_health_bars() -> void:
	if is_local_player():
		var spercent := float(shields) / MAX_SHIELDS * 100
		var hpercent := float(health) / MAX_HEALTH * 100
		hud.update_health_bars(hpercent, spercent)


func player_disconnected(id) -> void:
	if str(id) == name and get_tree().is_network_server():
		print("PLAYER DISCONNECTED")
		rpc("die")
		die()


func get_aim_pos(length: float) -> Vector3:
	var m_pos := window_res/2 + Vector2(-yaw, pitch)
	var from: Vector3 = camera.project_ray_origin(m_pos)
	var pos: Vector3 = from + camera.project_ray_normal(m_pos) * length
	var result := cast_ray(from, pos)

	if result:
		return result["position"]
	else:
		return pos


func _input(event) -> void:
	if event is InputEventMouseMotion:
		yaw -= event.relative.x * mouse_sensitivity
		yaw = clamp(yaw, window_res.x * -0.5, window_res.x * 0.5)
		pitch += event.relative.y * mouse_sensitivity
		pitch = clamp(pitch, window_res.y * -0.5, window_res.y * 0.5)

	if event.is_action_pressed("release_target"):
		self.target = null

	if event.is_action_pressed("secondary_fire"):
		if MultiplayerHandler.is_singleplayer():
			get_active_weapon(Weapon.FiringModes.SECONDARY_FIRE).fire(target)
		else:
			get_active_weapon(Weapon.FiringModes.SECONDARY_FIRE).rpc_unreliable("fire", target)

	if event.is_action_pressed("weapon_cycle_up"):
		cycle_active_weapon(Weapon.FiringModes.PRIMARY_FIRE, 1)
	elif event.is_action_pressed("weapon_cycle_down"):
		cycle_active_weapon(Weapon.FiringModes.PRIMARY_FIRE, -1)

# Get the closest enemy from an array of overlapping areas.
# This function really needs a name change
func get_closest_enemy_from_markers(enemy_markers: Array) -> Spatial:
	var closest: Spatial = enemy_markers[0].get_meta("entity")
	var pos := global_transform.origin
	var closest_dist := pos.distance_squared_to(closest.global_transform.origin)
	for sprite in enemy_markers:
		var enemy: Spatial = sprite.get_meta("entity")
		var enemy_dist := enemy.global_transform.origin.distance_squared_to(pos)
		if enemy_dist < closest_dist:
			closest_dist = enemy_dist
			closest = enemy
	return closest


func is_local_player() -> bool:
	# The name is actually a lie, check if we are IN CONTROL or are in singleplayer.
	# EVERY player is NOT local but instead handled by the Server.
	return !get_tree().has_network_peer() or (get_name() == str(MultiplayerHandler.nuid))


func _on_window_size_change() -> void:
	window_res = OS.window_size
	yaw = 0
	pitch = 0
	update_reticle()


# This function is mainly for animations.
func toggle_input_block() -> void:
	movement_blocked = !movement_blocked
	mouse_ignored = !mouse_ignored

	if mouse_ignored:
		yaw = 0
		pitch = 0
	if is_local_player():
		update_reticle()


func _set_target(t: Actor) -> void:
	target = t
	if get_tree().has_network_peer() and is_local_player():
		rpc("_set_target_from_path", t.get_path())


remote func _set_target_from_path(path: NodePath) -> void:
	self.target = get_node_or_null(path)


func _hud_targeted_node(actor: Actor) -> void:
	target = actor


func cycle_active_weapon(firing_mode: int, cycle_dir: int) -> void:
	# Check if the index would flow over the array size:
	var idx = active_weapons_indeces[firing_mode] + cycle_dir
	if idx >= weapons[firing_mode].size():
		idx -= weapons[firing_mode].size()
	elif idx < 0:
		idx += weapons[firing_mode].size()
	active_weapons_indeces[firing_mode] = idx
