extends Node

const MISSION_PATH = "res://missions/%s.json"
# Projectile stuff.
const PROJECTILE_LIST = preload("res://projectiles/projectile_list.gd")
var pl := PROJECTILE_LIST.new()
var projectiles_dict := pl.projectiles
var projectiles_array := projectiles_dict.values()


func _ready() -> void:
	# Assign a numeric id to the projectile, used to avoid string comparisons when pooling
	var i = 0
	for projectile in projectiles_array:
		projectile["id"] = i
		i += 1


func get_projectile_by_id(id: int) -> Dictionary:
	if id >= projectiles_array.size():
		print("[ERROR] Projectile ID '%s' is out of bounds." % id)
		return {}
	var projectile_dict = projectiles_array[id]
	if not projectile_dict.has("model_resource"):
		projectile_dict["model_resource"] = load(projectile_dict["model_resource_path"])
	return projectile_dict


func get_projectile_by_name(projectile_name: String) -> Dictionary:
	if not projectiles_dict.has(projectile_name):
		print("[ERROR] Invalid projectile '%s' requested." % projectile_name)
		return {}
	var projectile_dict = projectiles_dict[projectile_name]
	if not projectile_dict.has("resource"):
		projectile_dict["model_resource"] = load(projectile_dict["model_resource_path"])
	return projectile_dict


func load_json_file(path):
	var f = File.new()
	var err = f.open(path, f.READ)
	assert(err == OK)
	var txt: String = f.get_as_text()
	f.close()
	return JSON.parse(txt).get_result()
