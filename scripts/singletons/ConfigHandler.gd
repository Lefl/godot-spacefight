extends Node

const CFG_FILE_PATH := "user://config.ini"
var config := ConfigFile.new()
var force_server := false


func _ready() -> void:
	var env_force_server := OS.get_environment("GSF_FORCE_SERVER")
	if env_force_server == "1":
		force_server = true

	var err = config.load(CFG_FILE_PATH)
	# Create the default config file if it doesn't exist yet.
	if err == ERR_FILE_NOT_FOUND or err == ERR_CANT_OPEN:
		print("[WARNING] Config file not found, creating new one...")
		setup_config()
		config.load(CFG_FILE_PATH)
	elif not err == OK:
		print("[ERROR] Could not open config file, error code: %s" % err)

	call_deferred("apply_config")


func apply_config() -> void:
	# [display]
	var screen_size = config.get_value("display", "screen_size", Vector2(1028, 600))
	var fullscreen = config.get_value("dispaly", "fullscreen", false)
	var vsync = config.get_value("dispaly", "fullscreen", false)
	OS.set_use_vsync(vsync)
	OS.set_window_fullscreen(fullscreen)
	OS.set_window_size(screen_size)

	# [multiplayer]
	MultiplayerHandler.local_info["name"] = config.get_value("multiplayer", "name", "unnamed")

	# [server]
	var is_server = config.get_value("server", "start_as_server", false)
	if force_server or is_server:
		var port = config.get_value("server", "port", 2000)
		var lobby = config.get_value("server", "default_lobby_scene")
		MultiplayerHandler.create_server(port)
		get_tree().change_scene(lobby)


func get_config(section, kname):
	return config.get_value(section, kname)


func set_config(section, kname, value) -> void:
	config.set_value(section, kname, value)


func save() -> void:
	config.save(CFG_FILE_PATH)


func setup_config() -> void:
	var f := File.new()
	f.open("res://default_config.ini", File.READ)
	var fstr := f.get_as_text()
	f.close()
	var err := f.open("user://config.ini", File.WRITE)
	if not err == OK:
		print("[ERROR] Failed to create config file, error code: %s" % err)
		return
	f.store_string(fstr)
	f.close()
	print("[INFO] Created default config file")
