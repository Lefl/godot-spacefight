extends Control

onready var mission_description := ($Description as Label)
onready var objectives_hud := ($Objectives as VBoxContainer)
const objective_label = preload("res://scenes/gui/mission_hud/objective_label.tscn")
var current_label_anim := 0
var mission: Mission


func init(mi: Mission) -> void:
	mission = mi
	mission_description.set_text(mission.description)
	create_stage_objective_labels()


func _update() -> void:
# This function "redraws" the objectives.
# Objective Labels are updated inside the objectives.
	clear_objective_labels()
	if (
		mission.current_stage is MissionStageEnd or
		mission.current_stage is MissionStageFail
		):
		Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	else:
		create_stage_objective_labels()
		(objectives_hud.get_child(0) as ObjectiveLabel).fadein()


func create_stage_objective_labels() -> void:
	mission.current_stage.connect("stage_started", self, "_start_fadein", [], CONNECT_ONESHOT)
	for objective in mission.current_stage.objectives:
		if objective.description == "":
			continue
		var label = objective_label.instance()
		objectives_hud.add_child(label)
		label.set_text(objective.description)
		objective.connect("objective_completed", label, "fadeout", [],  CONNECT_ONESHOT)
		label.connect("faded_in", self, "_fadein_next", [], CONNECT_ONESHOT)
		objective.hud_info = label


func clear_objective_labels() -> void:
	for objective in objectives_hud.get_children():
		objective.free()


func _start_fadein() -> void:
	(objectives_hud.get_child(0) as ObjectiveLabel).fadein()


func _fadein_next() -> void:
	current_label_anim += 1
	if current_label_anim < objectives_hud.get_child_count():
		(objectives_hud.get_child(current_label_anim) as ObjectiveLabel).fadein()
