extends Control


signal targeted_node
enum {HOSTILE, ALLY, OBJECTIVE}
const MARKER_RES = preload("res://scenes/gui/ship_hud/marker/hud_marker.tscn")

onready var reticle := (get_parent().get_node("Reticle") as Control)
var hostile_factions = PoolStringArray(["reg_frac"])
var allied_factions = PoolStringArray(["faction_ipc"])

export (Color) var hostile_color
export (Color) var ally_color
export (Color) var objective_color

var markers := {}
var targeted_marker: HUDMarker


func _process(_delta: float) -> void:
	create_markers()
	update_markers()


func update_markers() -> void:
	var camera := get_tree().get_root().get_camera()
	for node in markers:
		var marker = (markers[node] as HUDMarker)
		var node_position := (node as Spatial).global_transform.origin
		var marker_position := camera.unproject_position(node_position)

		if camera.is_position_behind(node_position):
			marker.visible = false
		else:
			marker.visible = true

			marker.rect_position = marker_position# - marker_size * 0.5


func create_markers() -> void:
	var hostiles := []
	for faction in hostile_factions:
		hostiles += get_tree().get_nodes_in_group(faction)

	var allies := []
	for faction in allied_factions:
		allies += get_tree().get_nodes_in_group(faction)
	var objectives = get_tree().get_nodes_in_group("objective")

	## Deduplicate arrays
	var new_array = []
	for node in hostiles:
		if node in objectives or not is_node_valid(node):
			continue
		else:
			new_array.append(node)
	hostiles = new_array

	new_array = []
	for node in allies:
		if node in objectives or not is_node_valid(node):
			continue
		else:
			new_array.append(node)

	## For every type create a marker if it does not exist.
	# This is ugly
	for node in hostiles:
		var marker := get_marker(node)
		if not marker:
			markers[node] = create_marker_for_node(node, HOSTILE)
		elif marker.type != ALLY:
			marker.type = ALLY
			marker.color = hostile_color

	for node in allies:
		var marker := get_marker(node)
		if not marker:
			markers[node] = create_marker_for_node(node, ALLY)
		elif marker.type != ALLY:
			marker.type = ALLY
			marker.color = ally_color

	for node in objectives:
		if not is_node_valid(node):
			continue
		var marker := get_marker(node)
		if not marker:
			markers[node] = create_marker_for_node(node, OBJECTIVE)
		elif marker.type != OBJECTIVE:
			marker.type = OBJECTIVE
			marker.color = objective_color


func create_marker_for_node(node: Spatial, type: int) -> HUDMarker:
	var marker_color := Color(0)
	match type:
		HOSTILE:
			marker_color = hostile_color
		ALLY:
			marker_color = ally_color
		OBJECTIVE:
			marker_color = objective_color

	var new_marker := (MARKER_RES.instance() as HUDMarker)

	markers[node] = new_marker
	new_marker.node = node
	new_marker.type = type
	new_marker.connect("targeted", self, "_marker_targeted")
	if node is Actor:
		node.connect("has_died", self, "_node_died", [], CONNECT_ONESHOT)
	add_child(new_marker)
	new_marker.color = marker_color

	return new_marker


func _node_died(node: Actor) -> void:
	if markers[node] == targeted_marker:
		targeted_marker = null
	markers[node].queue_free()
	markers.erase(node)


func get_marker(node: Spatial) -> HUDMarker:
	if node in markers:
		return markers[node]
	else:
		return null


func _input(event: InputEvent) -> void:
	if event.is_action_released("lock_target"):
		var new_targeted_marker := target_marker()
		if new_targeted_marker:
			if targeted_marker:
				targeted_marker.unlock()
			new_targeted_marker.lockon()
			targeted_marker = new_targeted_marker
	elif event.is_action_released("release_target"):
		targeted_marker.unlock()
		targeted_marker = null


func target_marker() -> HUDMarker:
	var closest: HUDMarker
	var closest_distance := 0.0
	var mouse_position := reticle.rect_global_position

	for node in markers:
		var marker := (markers[node] as HUDMarker)
		# If the associated node is not hostile, continue
		if not node_in_faction(node, hostile_factions) or not marker.visible:
			continue
		# If there's no closest marker (first run) set one
		if not closest:
			closest = marker
			closest_distance = closest.rect_global_position.distance_squared_to(mouse_position)
			continue
		if targeted_marker == marker:
			continue

		var marker_position := marker.rect_global_position
		var distance_to_mouse := marker_position.distance_squared_to(mouse_position)
		if distance_to_mouse < closest_distance:
			closest = marker
			closest_distance = distance_to_mouse
	return closest


func _marker_targeted(marker: HUDMarker) -> void:
	emit_signal("targeted_node", marker.node)


func is_node_valid(node: Spatial) -> bool:
	if not node.visible or not is_instance_valid(node):
		return false
	else:
		return true


func node_in_faction(node: Spatial, factions: PoolStringArray) -> bool:
	for faction in factions:
		if node.is_in_group(faction):
			return true
	return false
