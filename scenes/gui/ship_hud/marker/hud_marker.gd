extends Control
class_name HUDMarker

signal targeted
onready var health := ($Health as TextureProgress)
onready var shield := ($Shields as TextureProgress)
onready var target := ($Target as Control)
onready var target_anim := (target.get_node("AnimationPlayer") as AnimationPlayer)
onready var tween := ($Tween as Tween)

const tween_speed = 1
var color: Color setget _set_colors
var node: Spatial

var type: int


func _ready() -> void:
	if (node as Actor):
		var a_node := (node as Actor)
		a_node.connect("recieved_damage", self, "_updated_health")
		if a_node.MAX_SHIELDS == 0:
			shield.value = 0
	else:
		shield.visible = false


func _updated_health(actor: Actor) -> void:
	tween_health(stepify(actor.health / actor.MAX_HEALTH * 100, 0.01))

	if actor.MAX_SHIELDS > 0:
		tween_shield(stepify(actor.shields / actor.MAX_SHIELDS * 100, 0.01))


func _set_colors(new_color: Color) -> void:
	health.tint_under = new_color
	health.tint_progress = new_color
	target.modulate = new_color
	color = color


func tween_health(new_value) -> void:
	tween.stop(health)
	tween.interpolate_property(
		health, "value", health.value, new_value, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN
		)
	tween.start()


func tween_shield(new_value) -> void:
	tween.stop(shield)
	tween.interpolate_property(
		shield, "value", shield.value, new_value, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN
		)
	tween.start()


func lockon() -> void:
	target_anim.play("lockon")

func unlock() -> void:
	target_anim.play("unlock")


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "lockon":
		emit_signal("targeted", self)
