#warnings-disable
extends Control


const hangar_lobby = preload("res://scenes/ui/multiplayer/hangar_lobby/hangar_lobby.tscn")
var masterserver
var masterserver_port
onready var login_status = $"Login/LoginStatus"
onready var start_status = $"Start/Status"


func _ready() -> void:
	masterserver = ConfigHandler.get_config("multiplayer", "masterserver")
	masterserver_port = ConfigHandler.get_config("multiplayer", "masterserver_port")

	MultiplayerHandler.connect("player_registered", self, "update_players")
	if not WebSocketHandler.client_info.empty() and not "username" in WebSocketHandler.client_info:
		login_status.set_text("Loaded credentials for %s. Press login to login" % WebSocketHandler.client_info["email"])
		$"Login/SaveLogin".pressed = true
		$"Login/LoginButton".set_disabled(false)
	elif "username" in WebSocketHandler.client_info:
		$"Login/LoginButton".set_disabled(true)
		login_status.set_text("Logged in as %s" % WebSocketHandler.client_info["username"])
		$"Start".set_disabled(true)


func _on_join_pressed() -> void:
	# Get the port and ip to connect to and create the client.
	var ip = $"DirectConnect/Join/Ip".get_text()
	var port = int($"DirectConnect/Join/Port".get_text())
	connect_to_server(ip, port)


func _on_host_pressed() -> void:
	# Get the port to host the server and call create_server.
	var port = int($"DirectConnect/Host/Port".get_text())
	var err = MultiplayerHandler.create_server(port)
	if err == OK:
		get_tree().change_scene_to(hangar_lobby)


func _on_email_text_entered(_new_text) -> void:
	$"Login/Password".grab_focus()

func _on_password_text_entered(_new_text) -> void:
	_on_login_pressed()


func _on_login_pressed() -> void:
	var email = $"Login/Email".get_text()
	var password = $"Login/Password".get_text()

	WebSocketHandler.init_masterserver_connection()
	yield(WebSocketHandler.wsc, "connection_established")

	if email and password:
		if not ("@" in email) or not ("." in email):
			login_status.set_text("INVALID EMAIL ADDRESS")
			return
		if len(password) <= 8:
			login_status.set_text("INVALID PASSWORD")
			return
		var save_login = $"Login/SaveLogin".is_pressed()
		WebSocketHandler.login_client(email, password, null, save_login)

	elif WebSocketHandler.client_info["login_token"]:
		var save_login = $"Login/SaveLogin".is_pressed()
		WebSocketHandler.login_client(
			WebSocketHandler.client_info["email"],
			null,
			WebSocketHandler.client_info["login_token"],
			save_login
		)
		WebSocketHandler.connect("websocket_authorized", self, "authorized")

	else:
		print("[WARNING] Either email or password or a token was not provided to login()!")
		return


func authorized() -> void:
	$"Start".set_disabled(false)
	login_status.set_text("Logged in as %s" % WebSocketHandler.client_info["username"])


func _on_start_pressed() -> void:
	WebSocketHandler.connect("received_server_info", self, "received_server_info")
	WebSocketHandler.send_message({"control": "request", "message": "server"})


func toggle_loading_anim() -> void:
	var loading_circle = $"start/loading_circle"
	loading_circle.set_visible(!loading_circle.is_visible())
	var anim = $"Start/AnimationPlayer"
	if not anim.is_playing():
		anim.play("loading")
	else:
		anim.stop()

func received_server_info(info) -> void:
	if typeof(info) != TYPE_DICTIONARY:
		$"start/status".set_text("No servers available!")
	else:
		connect_to_server(info["address"], info["port"])


func connect_to_server(address, port=null) -> void:
	if not port:
		port = 2000
	if not address:
		return

	var err = MultiplayerHandler.create_client(address, port)
	if err == OK:
		get_tree().connect("connected_to_server", self, "_connection_successful")
		get_tree().connect("connection_failed", self, "_connection_failed")
		start_status.set_text("Connecting to server...")
		toggle_loading_anim()


func _connection_successful() -> void:
	get_tree().change_scene_to(hangar_lobby)
	toggle_loading_anim()


func _connection_failed() -> void:
	start_status.set_text("Failed to connect to server")
	print("[ERROR] Failed to connect to server")
	toggle_loading_anim()
